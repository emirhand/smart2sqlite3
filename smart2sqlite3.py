"""smart2sqlite3 module file. 

Copyright 2018 M. Emirhan Döngel
GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>."""

import re
import sqlite3
from datetime import date

class SmartInfo():
    def __init__(self, logContent):
        """These are the values to look for in the lines. When detected, relevant text processing will be done on the line for the variables. """

        # To detect lines with basic disk info
        self.serialNoLineText = 'Serial Number'
        self.modelNoLineText = 'Device Model'
        self.capacityLineText = 'User Capacity'

        # To detect where the actual SMART attribute table resides
        self.attrStartLineText = re.compile('^ID#.+RAW_VALUE$') # start of SMART attribute table
        self.attrEndLineText = 'SMART Error Log Version: 1' # end of SMART attribute table

        """ We also record the lifetime value, which shows the age of disk at the time of long test. This is later used to determine if the test logs in question are recorded previously. """
        self.lifetimeStartLineText = re.compile('Extended.+offline.+Completed.+without.+error')

        self.serialNo = ''
        self.modelNo = ''
        self.capacity = 0
        self.lifetime = 0
        self.attrfirstline = 0
        self.attrlastline = 0

        """Smart attribute headers - to find starting positions of attribute values. 
        As individual disks might present differing strings, regular splitting methods did not work. 
Prep a list for them as we might use them a lot. """
        self.smartAttrHeaders = ['ID', 'ATTRIBUTE_NAME', 'FLAG', 'VALUE', 'WORST', 'THRESH', 'TYPE', 'UPDATED', 'WHEN_FAILED', 'RAW_VALUE']

        self.smartAttrHeaderDict = {attr: re.compile(attr, re.IGNORECASE) for attr in self.smartAttrHeaders}
        self.attrHeaderIndices = {}
        """Define a list to hold actual SMART attribute table. """
        self.smartAttrList = []
        """Define log /content/ as string once, filled in parse_disk_info"""
        self.content = logContent.splitlines()
        self.parse_disk_info()
        self.parse_smart_attrs()


    def parse_disk_info(self):
        """Function to parse elementary information about the disk and also the smart attribute lines for processing later. 
        Accepts the SMART log file content. Returns a dictionary of relevant values. """
        for line in self.content:
            # Is serial number info in this line?
            if self.serialNoLineText in line:
                # if so, it's in the third item of this line
                self.serialNo = "".join(line.split()[2])
            elif self.modelNoLineText in line:
                # Model no could have blank characters, so get until the end with blanks. 
                self.modelNo = " ".join(line.split()[2:])
            elif self.capacityLineText in line:
                # Capacity in bytes, including separating dots. 
                self.capacity = "".join(line.split()[2])
            elif self.lifetimeStartLineText.search(line):
                self.lifetime = "".join(line.split()[8])
            elif self.attrStartLineText.search(line):
                # The first line of SMART attributes
                self.attrfirstline = self.content.index(line) + 1
                # Starting points for all SMART table headers
                for key in self.smartAttrHeaderDict:
                    self.attrHeaderIndices[(key.lower())] = self.smartAttrHeaderDict[key].search(line).span()[0]
            
            elif self.attrEndLineText in line:
                # Finally, mark the end of the SMART attribute table
                self.attrlastline = self.content.index(line) - 1
        
    def parse_smart_attrs(self):
        """Parse the actual SMART attribute table. Takes the log content as input. A list of SMART attribute table lines is returned, with each line a sublist. """
        for line in self.content[self.attrfirstline:self.attrlastline]: 
            subdict = {}
            if line:
                """We know where each value begins from parse_disk_info function. Each one ends where the other starts. We strip the whitespaces to standardize. """
                for i in range(len(self.smartAttrHeaders)-1, -1, -1):
                    header = self.smartAttrHeaders[i].lower()
                    subdict[header] = line[self.attrHeaderIndices[header]:].strip()
                    line = line[0:self.attrHeaderIndices[header]]
            """...and append that subdict (attribute table line) into the main list. """
            self.smartAttrList.append(subdict)

def processDb(smartInfo, dbname='smart2sqlite3.sqlite3'):
    # If db file exists, open it. Else create one.
    conn = sqlite3.connect(dbname)
    cursor = conn.cursor()
    # Create tables if they don't exist
    sql_createdrivesTable = """CREATE TABLE IF NOT EXISTS `drives` ( `serialno` TEXT NOT NULL UNIQUE, `modelno` TEXT, `capacity` INTEGER, `inuse` INTEGER, `wheninstalled` TEXT, `whenremoved` TEXT, PRIMARY KEY(`serialno`) )"""
    sql_createdrivestatsTable = """CREATE TABLE IF NOT EXISTS `drivestats` ( `serialno` TEXT NOT NULL, `recordtime` TEXT NOT NULL, `lifetime` INTEGER, `id` INTEGER NOT NULL, `attribute_name` INTEGER, `flag` INTEGER, `value` INTEGER, `worst` INTEGER, `thresh` INTEGER, `type` INTEGER, `updated` INTEGER, `when_failed` INTEGER, `raw_value` INTEGER,  PRIMARY KEY (serialno, lifetime, id) )"""
    cursor.execute(sql_createdrivesTable)
    cursor.execute(sql_createdrivestatsTable)
    conn.commit()
    # Prepare drive info string from smartInfo
    sql_driveInfoString = """INSERT OR IGNORE INTO drives (serialno, modelno, capacity) VALUES ('{}', '{}', '{}')""".format(smartInfo.serialNo, smartInfo.modelNo, smartInfo.capacity.replace('.', ""))

    cursor.execute(sql_driveInfoString)
    """Generate and run SQL query string for SMART attributes. Could use some clever tricks to avoid repetition but this dumb code is easier to understand"""
    for line in smartInfo.smartAttrList:
        sql_smartAttrString = """INSERT OR IGNORE INTO drivestats (serialno, recordtime, lifetime, id, attribute_name, flag, value, worst, thresh, type, updated, when_failed, raw_value) VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')""".format(smartInfo.serialNo, date.today(), smartInfo.lifetime, line['id'], line['attribute_name'], line['flag'], line['value'], line['worst'], line['thresh'], line['type'], line['updated'], line['when_failed'], line['raw_value'])
        cursor.execute(sql_smartAttrString)
    conn.commit()
    conn.close()
