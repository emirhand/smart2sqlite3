# smart2sqlite3

A *very* experimental project, created while re-learning Python. Also my first public project. Be careful and preferably inspect & modify if you plan to use it in production. 

LICENSING INFORMATION

Copyright 2018 M. Emirhan Döngel
GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

# SMART2SQLITE3

This is my attempt at a parser for S.M.A.R.T data for hard drives. Current iteration parses the text output from 'smartctl -a /dev/sdX' command for use in an SQLite database file. 

## How to use as a standalone script

The main program will now take a single filename or a directory name, in addition to a database name with appropriate switches. The format for input files must be an unmodified smartctl -a output, which is tested for smartctl v6.6. 

Parsing is blindly done -my todo list has some intelligence to add. 

If the database file given does not exist, the file and the two tables will be created accordingly. Then (or if the file already exists) and if the following information is not already in the drives table, it will be written into the database: 

- Hard drive serial number
- Hard drive model number
- Hard drive capacity

The fields inuse, wheninstalled and whenremoved are reserved for future use. 

The drivestats table is for hard drive SMART attributes. This table holds the results for the most current SMART long test, if available. The primary keys are serialno, recordtime and lifetime fields. This avoids duplicate entries if the last test was already recorded. 

# How to use as a module

Import smart2sqlite3, then create an instance of smart2sqlite3.SmartInfo. This accepts a string, of course formatted as in smartctl -a output. 

smart2sqlite3.processDb function accepts a SmartInfo object and a database filename and writes information as described in the standalone usage notes. 
