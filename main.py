"""smart2sqlite3 main program code. 

Copyright 2018 M. Emirhan Döngel
GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. """

import smart2sqlite3
import argparse
import sys
import os
import configparser
import re

def main():
    config = configparser.ConfigParser()
    config.read('smart2sqlite3.conf')
    
    # If input comes from pipe, 
    inputArgs = sys.argv[1:]
    if not inputArgs:
        instring = sys.stdin.read()
        dbname = "smart2sqlite3.sqlite3"
        processInput(instring, dbName = dbname)
        sys.exit("Piped input processed. ")
        

    # If input is not piped, let's process arguments
    else: 
        argParser = argparse.ArgumentParser()
        argParser.add_argument("-s", "--database", help="Path to database file. The file will be created if it doesn't exist.")
        argParser.add_argument("-i", "--inputfile", help="Path to the SMART attribute file. This file should be a pure output from the smartctl -a <disk> command. ")
        argParser.add_argument("-d", "--directory", help="A directory that contains your SMART long test results file. All files must be pure text outputs from smartctl -a <disk> command. Will do my best to silently ignore non-conforming files. Can not be used with -i switch")
        args = argParser.parse_args()
        # Check for illegal switches
        if args.inputfile and args.directory:
            sys.exit("""Please use only one of -i and -d switches. """)
        if args.database:
            dbname = os.path.realpath(args.database)
        else:
            dbname = os.path.realpath(config["DEFAULT"]["dbfile"])
            
        """Now do processing. """    
        if args.inputfile:
            try:
                with open(args.inputfile, 'r') as infile:
                    instring = infile.read()
            except:
                sys.exit("Please check input filename. ")

            validity = checkSanity(instring, config)
            if validity != 7:
                print("Validity: {}. This does not seem to be a valid smartctl output".format(validity))
                sys.exit("Input file appears corrupted. ")
            print(dbname)
            processInput(instring, dbname)
        if args.directory and os.path.isdir(args.directory):
            for file in os.listdir(args.directory):
                with open(args.directory + '/' + file, 'r') as infile:
                    instring = infile.read()
                if checkSanity(instring, config) == 7:
                    processInput(instring, args.database)

        
def processInput(inString, dbName):
    smartInfo = smart2sqlite3.SmartInfo(inString)
    smart2sqlite3.processDb(smartInfo, dbname = os.path.realpath(dbName))

def checkSanity(inString, config):
    infoLine = re.compile(config["DEFAULT"]["infoLine"])
    infoSection = re.compile(config["DEFAULT"]["infoSection"])
    dataSection = re.compile(config["DEFAULT"]["dataSection"])
    validity = 0
    if infoLine.match(inString):
        validity += 1
    if infoSection.search(inString):
        validity += 2
    if dataSection.search(inString):
        validity += 4

    return validity
    
    
if __name__ == "__main__":
    main()
